package com.company;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

/**
 * This program allows to scan and edit ModBus registers in a network.
 *
 * @author Alberto Tessari - VR447763
 * @author Filippo Nevi - VR458510
 */
public class Main {
    static ModbusInteractor mi;

    /**
     *
     * Main function. It defines and instantiates a ModbusInteractor object and a menu.
     *
     * @param args
     */
    public static void main(String[] args) {
        mi = new ModbusInteractor();
        boolean keepAlive = true;
        boolean quitAfterFile = false;
        ArrayList<String> input = new ArrayList<String>();
        if (args.length > 0){
            String path = args[0];
            File tempFile = new File(path);
            boolean exists = tempFile.exists();
            if (!exists){
                System.out.println("Error! File '" + path + "' does not exist");
            }
            else{
                try{
                    File file=new File(path);    //creates a new file instance
                    FileReader fr=new FileReader(file);   //reads the file
                    BufferedReader br=new BufferedReader(fr);  //creates a buffering character input stream
                    String line;
                    while((line=br.readLine())!=null)
                    {
                        input.add(line);
                        if((line.contains("quit")) || (line.contains("exit"))) {
                            quitAfterFile = true;
                        }
                    }
                    fr.close();    //closes the stream and release the resources
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }
            }
        }

        while(keepAlive){
            for (String in: input){
                menu(in);
            }
            if (quitAfterFile) {
                keepAlive = false;
            } else {
                System.out.print("\n$ ");
                Scanner sc = new Scanner(System.in);
                String manual_input = sc.nextLine();
                keepAlive = menu(manual_input);
            }
        }
    }

    /**
     *
     * Text menu. The user can interact with it by using a fixed set of commands.
     *
     * @return False if 'quit' or 'exit' commands are submit, True otherwise.
     */
    public static boolean menu(String input) {


        String splitInput[] = input.split(" ");
        String command = splitInput[0];
        int lengths[];
        int length = 0;
        switch (command) {
            case "exit":
            case "quit":
                return false;
            case "h":
            case "help":
            case "?":
                printHelp();
                break;
            case "sp":
            case "setParameters":
                lengths = new int []{3, 5, 7};
                length = splitInput.length;
                if (!contains(lengths, length)){
                    print("setParameters error: invalid number of arguments!");
                    return true;
                }
                for (int i = 0; i < length; i ++){
                    switch (splitInput[i]){
                        case "-ip":
                        case "-i":
                            if (i == length-1){
                                print("setParameters error: -ip should be followed by an address");
                                return true;
                            }
                            mi.setIp(splitInput[i+1]);
                            break;
                        case "-port":
                        case "-p":
                            if (i == length-1){
                                print("setParameters error: -port should be followed by a port number");
                                return true;
                            }
                            mi.setPort(Integer.parseInt(splitInput[i+1]));
                            break;
                        case "-unitid":
                        case "-u":
                            if (i == length-1){
                                print("setParameters error: -unitid should be followed by a port number");
                                return true;
                            }
                            mi.setUnitId(Integer.parseInt(splitInput[i+1]));
                            break;
                    }
                }
                break;
            case "readInputregisters":
            case "rir":
                //int[] lengths, int length, String splitInput[], boolean holding
                read_registers(splitInput, false);
                break;
            case "readHoldingRegister":
            case "rhr":
                read_registers(splitInput, true);
                break;
            case "readCoilRegister":
            case "rcr":
                read_boolean_registers(splitInput, true);
                break;
            case "readDiscreteInputRegister":
            case "rdir":
                read_boolean_registers(splitInput, false);
                break;
            case "scanRegisters":
            case "srs":
                scan_registers(splitInput);
                break;
            case "scan":
                lengths = new int[]{3};
                length = splitInput.length;
                String startingIp = "";

                if (!contains(lengths, length)){
                    print("scan error: invalid number of arguments!");
                    return true;
                }

                for (int i = 0; i < length; i ++)
                {
                    switch (splitInput[i]) {
                        case "-start":
                        case "-s":
                            if (i == length - 1) {
                                print("scan error: -start should be followed by an address");
                            }
                            startingIp = splitInput[i+1];
                            break;
                    }
                }
                int devices = mi.scanNetwork(startingIp);
                print("SCAN COMPLETE: " + Integer.toString(devices) + " devices found!");
                break;
            case "setCoil":
            case "sc":
                lengths = new int[]{5, 7, 9};
                length = splitInput.length;
                if (!contains(lengths, length)){
                    print("setCoil error: invalid number of arguments!");
                    return true;
                }
                int coil = -1;
                boolean coilValue = false, isValueSet = false;
                for (int i = 0; i < length; i ++) {
                    switch (splitInput[i]) { // ip, port, register, length
                        case "-ip":
                        case "-i":
                            if (i == length - 1) {
                                print("setCoil error: -ip should be followed by an address");
                                return true;
                            }
                            mi.setIp(splitInput[i + 1]);
                            break;
                        case "-port":
                        case "-p":
                            if (i == length - 1) {
                                print("setCoil error: -port should be followed by a port number");
                                return true;
                            }
                            mi.setPort(Integer.parseInt(splitInput[i + 1]));
                            break;
                        case "-coil":
                        case "-c":
                            if (i == length - 1) {
                                print("setCoil error: -coil should be followed by a coil number");
                                return true;
                            }
                            coil = Integer.parseInt(splitInput[i + 1]);
                            break;
                        case "-value":
                        case "-v":
                            if (i == length - 1) {
                                print("setCoil error: -value should be followed by a boolean or an integer between 0 or 1");
                                return true;
                            }
                            coilValue = Boolean.parseBoolean(splitInput[i + 1]);
                            isValueSet = true;
                            break;
                    }
                }
                if ((coil != -1) && (isValueSet = true)) {
                    boolean operation = mi.setCoil(coil, coilValue);
                    if (operation)
                        print("Coil value set!");
                    else
                        print("There was an error setting the value of the coil.");
                }
                break;
            case "setRegister":
            case "sr":
                lengths = new int[]{5, 7, 9};
                length = splitInput.length;
                if (!contains(lengths, length)){
                    print("setRegister error: invalid number of arguments!");
                    return true;
                }
                int register = -1;
                int regValue = 0;
                isValueSet = false;
                for (int i = 0; i < length; i ++) {
                    switch (splitInput[i]) { // ip, port, register, length
                        case "-ip":
                        case "-i":
                            if (i == length - 1) {
                                print("setRegister error: -ip should be followed by an address");
                                return true;
                            }
                            mi.setIp(splitInput[i + 1]);
                            break;
                        case "-port":
                        case "-p":
                            if (i == length - 1) {
                                print("setRegister error: -port should be followed by a port number");
                                return true;
                            }
                            mi.setPort(Integer.parseInt(splitInput[i + 1]));
                            break;
                        case "-register":
                        case "-r":
                            if (i == length - 1) {
                                print("setRegister error: -register should be followed by a register number");
                                return true;
                            }
                            register = Integer.parseInt(splitInput[i + 1]);
                            break;
                        case "-value":
                        case "-v":
                            if (i == length - 1) {
                                print("setRegister error: -value should be followed by an integer number");
                                return true;
                            }
                            regValue = Integer.parseInt(splitInput[i + 1]);
                            isValueSet = true;
                            break;
                    }
                }
                if ((register != -1) && (isValueSet = true)) {
                    boolean operation = mi.setHoldingRegister(register, regValue);
                    if (operation) {
                        print("Register value set!");
                    } else {
                        print("There was an error setting the value of the register.");
                    }
                }
                break;
        }
        return true;
    }

    /**
     * Shows which commands can be used and how.
     */
    public static void printHelp () {
        print("AVAILABLE COMMANDS\ncommand - description $ usage");
        print("setParameters - set ip address, port and unitid of the modbus connection $ sp [-ip 192.168.0.1] [-port 502] [-unitid 0]");
        print("readInputRegisters - returns the value of the selected input registers $ rir [-ip 192.168.0.1] [-port 502] [-register 10] [-length 2]");
        print("readHoldingRegisters - returns the value of the selected holding registers $ rhr [-ip 192.168.0.1] [-port 502] [-register 10] [-length 2]");
        print("readCoilRegisters - returns the value of the selected coil registers $ rcr [-ip 192.168.0.1] [-port 502] [-register 10] [-length 2]");
        print("readDiscreteInputRegisters - returns the value of the selected discrete input registers $ rdir [-ip 192.168.0.1] [-port 502] [-register 10] [-length 2]");
        print("scan - returns the number of modbus devices found on the specified network ip range $ scan -start 192.168.0.1");
        print("setCoil - set the value of a coil $ sc [-ip 192.168.0.1] [-port 502] -coil 10 -value false");
        print("setRegister - set the value of a register $ sr [-ip 192.168.0.1] [-port 502] -register 10 -value 289");
        print("scanRegisters - reads the value of all the registers of the given type in the given interval (available scan types: holding, input, discreteInput, coil) $ srs [-ip 192.168.0.1] [-port 502] -rtype holding [-first 0] -last 10");
    }

    /**
     *
     * Encapsulates the function System.out.println();
     *
     * @param string String to be printed.
     */
    public static void print (String string){
        System.out.println(string);
    }

    /**
     *
     * Checks if a key is contained in an array.
     *
     * @param arr Array to be checked.
     * @param key Key to be searched.
     * @return True if the key is contained in the array, False otherwise.
     */
    public static boolean contains(final int[] arr, final int key) {
        return Arrays.stream(arr).anyMatch(i -> i == key);
    }

    public static boolean scan_registers(String splitInput[]){
        int[] lengths = new int[]{3, 5, 7, 9, 11};
        String rtype = "";
        int length = splitInput.length;
        int first = 0;
        int last = 1;
        if (!contains(lengths, length)){
            print("scanRegisters error: invalid number of arguments!");
            return true;
        }
        for (int i = 0; i < length; i ++){
            switch (splitInput[i]){ // ip, port, register, length
                case "-ip":
                case "-i":
                    if (i == length-1){
                        print("scanRegisters error: -ip should be followed by an address");
                        return true;
                    }
                    mi.setIp(splitInput[i+1]);
                    break;
                case "-port":
                case "-p":
                    if (i == length-1){
                        print("scanRegisters error: -port should be followed by a port number");
                        return true;
                    }
                    mi.setPort(Integer.parseInt(splitInput[i+1]));
                case "-first":
                case "-f":
                    if (i == length-1){
                        print("scanRegisters error: -register should be followed by a register number");
                        return true;
                    }
                    first = Integer.parseInt(splitInput[i+1]);
                    break;
                case "-last":
                case "-l":
                    if (i == length-1){
                        print("scanRegisters error: -length should be followed by a number");
                        return true;
                    }
                    last = Integer.parseInt(splitInput[i+1]);
                    break;
                case "-rtype":
                case "rt":
                    if (i == length -1){
                        print("scanRegisters error: -rtype should be followed by a string");
                        return true;
                    }
                    rtype = splitInput[i+1];
            }
        }

        if (first < 0 || last < 0){
            print("Error readRegister: illegal (first or last) register number!");
            return true;
        }
        int counter = first;
        int result;
        boolean b_result;
        while (counter <= last){

            switch (rtype) {
                case "holding":
                    result = mi.readSingleHolding(counter);
                    System.out.println(counter + ": [" + result + "]");
                    break;
                case "input":
                    result = mi.readSingleInput(counter);
                    System.out.println(counter + ": [" + result + "]");
                    break;
                case "coil":
                    b_result = mi.readSingleCoil(counter);
                    System.out.println(counter + ": [" + b_result + "]");
                    break;
                case "discreteInput":
                case "discreteinput":
                case "discrete":
                    b_result = mi.readSingleDiscreteInput(counter);
                    System.out.println(counter + ": [" + b_result + "]");
                    break;
                default:
                    System.out.println("rtype not recognized, the available types are:");
                    System.out.println("holding, coil, input, discreteInput");
                    return true;

            }
            counter ++;
        }
        return true;
    }

    public static boolean read_registers(String splitInput[], boolean holding){
        int[] lengths = new int[]{3, 5, 7, 9};
        int length = splitInput.length;
        int register = 0;
        int readLength = 1;
        if (!contains(lengths, length)){
            if (holding)
                print("readHoldingRgisters error: invalid number of arguments!");
            else
                print("readInputRgisters error: invalid number of arguments!");
            return true;
        }
        for (int i = 0; i < length; i ++){
            switch (splitInput[i]){ // ip, port, register, length
                case "-ip":
                case "-i":
                    if (i == length-1){
                        if (holding)
                            print("readHoldingReisters error: -ip should be followed by an address");
                        else
                            print("readInputReisters error: -ip should be followed by an address");
                        return true;
                    }
                    mi.setIp(splitInput[i+1]);
                    break;
                case "-port":
                case "-p":
                    if (i == length-1){
                        if (holding)
                            print("readHoldingReisters error: -port should be followed by a port number");
                        else
                            print("readInputReisters error: -port should be followed by a port number");
                        return true;
                    }
                    mi.setPort(Integer.parseInt(splitInput[i+1]));
                case "-register":
                case "-r":
                    if (i == length-1){
                        if (holding)
                            print("readHoldingReisters error: -register should be followed by a register number");
                        else
                            print("readInputReisters error: -register should be followed by a register number");
                        return true;
                    }
                    register = Integer.parseInt(splitInput[i+1]);
                    break;
                case "-length":
                case "-l":
                    if (i == length-1){
                        if (holding)
                            print("readHoldingReisters error: -length should be followed by number");
                        else
                            print("readInputReisters error: -length should be followed by number");
                        return true;
                    }
                    readLength = Integer.parseInt(splitInput[i+1]);
                    break;
            }
        }

        if (register < 0){
            print("Error readRegister: illegal register number!");
            return true;
        }
        if (readLength > 10){
            print("Warning: the maximum reading length available is 10, using it instead of: " + readLength);
            readLength = 10;
        }
        int counter = 0;
        if (holding){
            int [] result = mi.readHolding(register, readLength);
            //print(mi.readHolding(register, readLength).toString());
            for (int value: result){
                System.out.println((register + counter) + ": [" + value + "] ");
                counter ++;
            }
        }
        else{
            int [] result = mi.readInput(register, readLength);
            for (int value: result){
                System.out.println((register + counter) + ": [" + value + "] ");
                counter ++;
            }
        }
        return true;
    }

    public static boolean read_boolean_registers(String splitInput[], boolean coil){
        int[] lengths = new int[]{3, 5, 7, 9};
        int length = splitInput.length;
        int register = 0;
        int readLength = 1;
        if (!contains(lengths, length)){
            if (coil)
                print("readCoilRgisters error: invalid number of arguments!");
            else
                print("readDiscreteInputRgisters error: invalid number of arguments!");
            return true;
        }
        for (int i = 0; i < length; i ++){
            switch (splitInput[i]){ // ip, port, register, length
                case "-ip":
                case "-i":
                    if (i == length-1){
                        if (coil)
                            print("readCoilRgisters error: -ip should be followed by an address");
                        else
                            print("readDiscreteInputRgisters error: -ip should be followed by an address");
                        return true;
                    }
                    mi.setIp(splitInput[i+1]);
                    break;
                case "-port":
                case "-p":
                    if (i == length-1){
                        if (coil)
                            print("readCoilRgisters error: -port should be followed by a port number");
                        else
                            print("readDiscreteInputRgisters error: -port should be followed by a port number");
                        return true;
                    }
                    mi.setPort(Integer.parseInt(splitInput[i+1]));
                case "-register":
                case "-r":
                    if (i == length-1){
                        if (coil)
                            print("readCoilRgisters error: -register should be followed by a register number");
                        else
                            print("readDiscreteInputRgisters error: -register should be followed by a register number");
                        return true;
                    }
                    register = Integer.parseInt(splitInput[i+1]);
                    break;
                case "-length":
                case "-l":
                    if (i == length-1){
                        if (coil)
                            print("readCoilRgisters error: -length should be followed by number");
                        else
                            print("readDiscreteInputRgisters error: -length should be followed by number");
                        return true;
                    }
                    readLength = Integer.parseInt(splitInput[i+1]);
                    break;
            }
        }

        if (register < 0){
            print("Error readRegister: illegal register number!");
            return true;
        }
        if (readLength > 10){
            print("Warning: the maximum reading length available is 10, using it instead of: " + readLength);
            readLength = 10;
        }
        int counter = 0;
        if (coil){
            boolean [] result = mi.readCoil(register, readLength);
            //print(mi.readHolding(register, readLength).toString());
            for (boolean value: result){
                System.out.println((register + counter) + ": [" + value + "] ");
                counter ++;
            }
        }
        else{
            boolean [] result = mi.readDiscreteInput(register, readLength);
            for (boolean value: result){
                System.out.println((register + counter) + ": [" + value + "] ");
                counter ++;
            }
        }
        return true;
    }

}

