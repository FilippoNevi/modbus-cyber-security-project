package com.company;

/**
 * This function shows the progress bar of a command, while the ModbusInteractor is scanning the network.
 */
public class ProgressBar {
    int length;

    /**
     * Constructor.
     * @param l Length of the bar.
     */
    ProgressBar(int l){
        this.length = l;
    }

    /**
     * Constructor. Sets the length to 70 as a default value.
     */
    ProgressBar(){
        this.length = 70;
    }

    /**
     * Prints the progress bar as a set of '#' or '-' symbols, depending on the amount of progress made.
     * @param progress Value of the progress to be shown; 0 means 'none', 1 means 'done'.
     */
    void progress(double progress){
        int barLength = this.length;
        String status = "";
        if (progress < 0) {
            progress = 0;
            status = "Halt...\r\n";
        }
        if (progress >= 1) {
            progress = 1;
            status = "Done...\r\n";
        }
        long block = Math.round(barLength*progress);
        //String text = "\rPercent: [{0}] {1}% {2}".format( "#"*block + "-"*(barLength-block), round(progress*100), status)
		//text = "\rExecuting clustering: [{0}]".format( "#"*block + "-"*(barLength-block))
        String text = "\rScanning: [";
        for (int i = 0; i < block; i ++)
            text += "#";
        for (int i = barLength; i > block; i--)
            text += "-";
        text += "]";
        text += " " + Math.round(progress*100) + "%";
        System.out.print(text);
        System.out.flush();
    }
}
