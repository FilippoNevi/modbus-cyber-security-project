package com.company;

import de.re.easymodbus.modbusclient.ModbusClient;

import java.util.Arrays;

/**
 * This class allows to manage a Modbus client.
 */
public class ModbusInteractor {
    private ModbusClient client;

    /**
     * Constructor.
     */
    ModbusInteractor() {
        System.out.println("Initializing modbus interactor");
        client = new ModbusClient();
        // initializing default values
        this.setPort(502);
        this.setUnitId(0);
        this.setIp("10.23.16.179");
    }

    /**
     * Sets the client ip.
     * @param ip String containing the ip.
     */
    public void setIp(String ip) {
        this.client.setipAddress(ip);
    }

    /**
     * @return Ip of the client.
     */
    public String getIP() {
        return this.client.getipAddress();
    }

    /**
     * Sets the port of the client.
     * @param port String containing the port.
     */
    public void setPort(int port) {
        this.client.setPort(port);
    }

    /**
     * Sets the id of the unit, in order to identify different devices on the network.
     * @param unitId index of the device
     */
    public void setUnitId(int unitId){
        this.client.setUnitIdentifier(this.client.toByteArray(unitId)[0]);
    }

    /**
     * @return Port of the client.
     */
    public int getPort() {
        return this.client.getPort();
    }

    /**
     * Connects the client to a server and handles the possible exceptions.
     * @return 0 if the connection gets established, -1 otherwise.
     */
    public int connect(){
        try {
            this.client.Connect();
            return 0;
        } catch (Exception e) {
            this.handleExceptions(e);
            return -1;
        }
    }

    /**
     * Disconnects the client from a server.
     */
    public void disconnect(){
        try{
            this.client.Disconnect();
        }catch(Exception e){
            this.handleExceptions(e);
        }
    }

    /**
     * Connects the client to a server, without handling any possible exception.
     * @return 0 if the connection gets established, -1 otherwise.
     */
    public int silentConnect(){
        try {
            this.client.Connect();
            return 0;
        } catch (Exception e) {
            return -1;
        }
    }

    /**
     * Connects the client to a specific ip.
     * @param ip Ip address of the server.
     * @return Outcome of the connection.
     */
    public int connect(String ip){
        this.setIp(ip);
        return this.connect();
    }

    /**
     * Connects the client to a specific port.
     * @param port Port of the server.
     * @return Outcome of the connection.
     */
    public int connect(int port){
        this.setPort(port);
        return this.connect();
    }

    /**
     * Connects the client to a specific ip and port combination.
     * @param ip Ip address of the server.
     * @param port Port of the server.
     * @return Outcome of the connection.
     */
    public int connect(String ip, int port) {
        this.setIp(ip);
        this.setPort(port);
        return this.connect();
    }

    /**
     * Reads a specific amount of Holding registers.
     * @param register Register address to start the read from.
     * @param length Length of the data to be read.
     * @return Array containing the data read from the registers.
     */
    public int[] readHolding(int register, int length) {
        if (this.connect() == -1)
            return new int[]{1};
        try {
            int [] result = this.client.ReadHoldingRegisters(register, length);
            this.disconnect();
            return result;
        } catch (Exception e) {
            this.handleExceptions(e);
        }
        return new int[]{1};
    }

    /**
     * Reads one value from a Holding register.
     * @param register Register address to be read.
     * @return Value read from the register.
     */
    public int readSingleHolding(int register){
        return this.readHolding(register, 1)[0];
    }

    /**
     * Reads a specific amount of Coils.
     * @param register Coil address to start the read from.
     * @param length Length of the data to be read.
     * @return Array containing the data read from the coils.
     */
    public boolean[] readCoil(int register, int length){
        if (this.connect() == -1)
            return new boolean []{false};
        try{
            boolean [] result = this.client.ReadCoils(register, length);
            this.disconnect();
            return result;
        }catch (Exception e){
            this.handleExceptions(e);
        }
        return new boolean[]{false};
    }

    /**
     * Reads a specific amount of Discrete Input registers.
     * @param register Register address to start the read from.
     * @param length Length of the data to be read.
     * @return Array containing the data read from the registers.
     */
    public boolean[] readDiscreteInput(int register, int length){
        if (this.connect() == -1)
            return new boolean []{false};
        try{
            boolean [] result = this.client.ReadDiscreteInputs(register, length);
            this.disconnect();
            return result;
        }catch (Exception e){
            this.handleExceptions(e);
        }
        return new boolean[]{false};
    }

    /**
     * Reads a specific amount of Read Input registers.
     * @param register Register address to start the read from.
     * @param length Length of the data to be read.
     * @return Array containing the data read from the registers.
     */
    public int[] readInput(int register, int length) {
        if (this.connect() == -1)
            return new int[]{1};
        try {
            int [] result = this.client.ReadInputRegisters(register, length);
            this.disconnect();
            return result;
        } catch (Exception e) {
            this.handleExceptions(e);
        }
        return new int[]{1};
    }

    /**
     * Reads one value from a Read Input register.
     * @param register Register address to be read.
     * @return Value read from the register.
     */
    public int readSingleInput(int register){
        return this.readInput(register, 1)[0];
    }

    /**
     * Reads one value from a Coil.
     * @param register Coil address to be read.
     * @return Value read from the coil.
     */
    public boolean readSingleCoil(int register){
        return this.readCoil(register, 1)[0];
    }

    /**
     * Read one value from a Discrete Input register.
     * @param register Register address to be read.
     * @return Value read from the register.
     */
    public boolean readSingleDiscreteInput(int register){
        return this.readDiscreteInput(register, 1)[0];
    }
    // expected address style: 192.168.0.0

    /**
     * Scans a network starting from an address and going on until reaching the last possible address (x.x.x.254).
     * @param startingAddress Initial address that the function starts scanning. It expects a string in the standard IPv4 format: 4 integers separated by dots.
     * @return Number of devices found in the network. Returns -1 if there are errors.
     */
    public int scanNetwork(String startingAddress){
        String parts[] = startingAddress.split("\\.");
        int devicesFound = 0;

        if (parts.length != 4){
            System.out.println("ERROR: the starting Address for a scan should have the standard format 'xxx.xxx.xxx.xxx'");
            System.out.println("entered ip: " + parts[0]);
            return -1;
        }

        String baseIp = parts[0] + "." + parts[1] + "." + parts[2] + ".";
        int counter;
        int startingCounter = 0;
        try{
            counter = Integer.parseInt(parts[3]);
            startingCounter = counter;
        }catch (Exception e){
            this.handleExceptions(e);
            counter = 0;
        }
        ProgressBar pb = new ProgressBar();
        pb.progress(0);
        while (counter < 255){
            String newIp = baseIp + Integer.toString(counter);

            this.setIp(newIp);
            int response = this.silentConnect();
            if (response == 0){
                System.out.println("\n\nMODBUS DEVICE FOUND, IP ADDRESS: " + newIp + "\n");
                devicesFound ++;
                this.disconnect();
            }
            counter ++;
            double normalizedStep = (double)(counter - startingCounter) / (255-startingCounter);
            pb.progress(normalizedStep);
        }
        System.out.println("\n");
        return devicesFound;
    }

    /**
     * Scans holding registers, given starting and ending addresses.
     * @param start Starting address of the scan.
     * @param end Ending address of the scan.
     * @return Values read from the holding registers.
     */
    public int[] scanHolding(int start, int end){
        if (this.connect() == -1)
            return new int[]{-1};
        int result [] = new int[end - start + 1];
        int index;
        int counter = start;
        while (counter <= end){
            index = counter - start;
            result[index] = this.readSingleHolding(counter);
            counter ++;
        }
        return result;
    }

    /**
     * Scans input registers, given starting and ending addresses.
     * @param start Starting address of the scan.
     * @param end Ending address of the scan.
     * @return Values read from the input registers.
     */
    public int[] scanInput(int start, int end){
        if (this.connect() == -1)
            return new int[]{-1};
        int result [] = new int[end - start + 1];
        int index;
        int counter = start;
        while (counter <= end){
            index = counter - start;
            result[index] = this.readSingleInput(counter);
            counter ++;
        }
        return result;
    }

    /**
     * Scans coils, given starting and ending addresses.
     * @param start Starting address of the scan.
     * @param end Ending address of the scan.
     * @return Values read from the coils.
     */
    public boolean[] scanCoil(int start, int end){
        if (this.connect() == -1)
            return new boolean[]{false};
        boolean result [] = new boolean[end - start + 1];
        int index;
        int counter = start;
        while (counter <= end){
            index = counter - start;
            result[index] = this.readSingleCoil(counter);
            counter ++;
        }
        return result;
    }

    /**
     * Scans discrete input registers, given starting and ending addresses.
     * @param start Starting address of the scan.
     * @param end Ending address of the scan.
     * @return Values read from the discrete input registers.
     */
    public boolean[] scanDiscreteInput(int start, int end){
        if (this.connect() == -1)
            return new boolean[]{false};
        boolean result [] = new boolean[end - start + 1];
        int index;
        int counter = start;
        while (counter <= end){
            index = counter - start;
            result[index] = this.readSingleDiscreteInput(counter);
            counter ++;
        }
        return result;
    }

    /**
     * Edits the value of a holding register.
     * @param register Address of the holding register.
     * @param value Value to be set.
     * @return Outcome of the operation.
     */
    public boolean setHoldingRegister(int register, int value) {
        if (this.connect() == -1)
            return false;

        try {
            this.client.WriteSingleRegister(register, value);
        } catch (Exception e) {
            this.handleExceptions(e);
            return false;
        }

        return true;
    }

    /**
     * Edits the value of a coil.
     * @param coil Address of the coil.
     * @param value Value to be set.
     * @return Outcome of the operation.
     */
    public boolean setCoil(int coil, boolean value) {
        if (this.connect() == -1) {
            return false;
        }

        try {
            this.client.WriteSingleCoil(coil, value);
        } catch (Exception e) {
            this.handleExceptions(e);
            return false;
        }
        return true;
    }

    /**
     * Handles a given exception.
     * @param ex Exception to be handled.
     */
    private void handleExceptions(Exception ex) {
        System.out.println("EXCEPTION RAISED AND HANDLED BY 'ModbusInteractor'");
        System.out.println("\t" + ex.getMessage());
        System.out.println("Stack trace: \n\t" + Arrays.toString(ex.getStackTrace()));
    }
}
